import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { withRedux } from '../lib/redux'
import Link from 'next/link'

const IndexPage = () => {

  const dispatch = useDispatch()
  const aboutInfoRedux = useSelector(state => state.news)

  const clickAbout = () => {
    return dispatch({
      type:'ABOUT_TEST'
    })
  }

  return (
    <div>
      <h1>index page</h1>
      <input onClick={clickAbout} type="submit" />
      <Link href="/about">
        <a>about</a>
      </Link>
    </div>
  )
}

IndexPage.getInitialProps = ({ }) => {

  return {}
}

export default withRedux(IndexPage)
