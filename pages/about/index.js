import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { withRedux } from '../../lib/redux'
import Link from 'next/link'

const AboutPage = () => {

  const dispatch = useDispatch()
  const clickCount = useSelector(state => state.news.click)

  return (
    <div>
      <h1>AboutPage { clickCount }</h1>
      <Link href="/">
        <a>home</a>
      </Link>
    </div>
  )
}

AboutPage.getInitialProps = ({ }) => {

  return {}
}

export default withRedux(AboutPage)
