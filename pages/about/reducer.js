const initialState = {
	about:'test',
	click:0
}

import actions from '../../Service/Api/Actions'

const news = (state = initialState, action) => {
	switch(action.type){
		case actions.news.ABOUT_TEST :
			return {
				...state,
				about:'test click',
				click: state.click + 1
			}
		default :
			return state
	}
}

export const newsInitialState = initialState
export default news
