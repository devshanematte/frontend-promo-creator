import { applyMiddleware, createStore } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
import { composeWithDevTools } from 'redux-devtools-extension'
import logger from 'redux-logger'
import reducers from './reducers'
import storage from 'redux-persist/lib/storage'
import initialState from './initial-state'

export const initializeStore = (preloadedState = initialState) => {

  const persistConfig = {
    key: 'root',
    storage,
    blacklist: [],
    timeout: null,
  }

  const middleware = []
  const enhancers = []

  middleware.push(logger)

  const pReducer = persistReducer(persistConfig, reducers)

  enhancers.push(applyMiddleware(...middleware))

  const store = createStore(pReducer, initialState, composeWithDevTools(...enhancers))
  const persistor = persistStore(store)

  return {
    store, persistor
  }

}
