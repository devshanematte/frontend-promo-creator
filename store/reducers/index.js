import { combineReducers } from 'redux'
import news from '../../pages/about/reducer'

export default combineReducers({
    news
})
