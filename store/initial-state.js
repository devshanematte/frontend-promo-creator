import { newsInitialState } from '../pages/about/reducer'

const initialState = {
    news:{
        ...newsInitialState
    }
}

export default initialState
